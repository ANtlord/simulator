// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    id: choice
    x:0
    y:0
    width: parent.width
    height: labelChoice.height+30

    states: [
        State {
            name: "checked"
            PropertyChanges {
                target: choice
                gradient: checkedGradient
            }
        },
        State {
            name: "choosed"
            PropertyChanges {
                target: choice
                gradient: gray
            }
        }
    ]

    Gradient {
        id: gray
        GradientStop {
            id: topGradient
            position: 0.0
            color: "#9c9d9c"
        }
        GradientStop {
            id: bottomGradient
            position: 1.0
            color: "#3b4439"
        }
    }

    property string liecolor: ""

    Text {
        id: labelChoice
        color: "white"
        font.family: "Arial"
        font.pixelSize: 13
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: choice.left
        anchors.leftMargin: 20
        width: parent.width-20
        wrapMode: Text.WordWrap
    }

    DoubleGradient {
        id: uncheckedGradient
    }

    DoubleGradient {
        id: checkedGradient
        topColor: "#18b703"
        bottomColor: "#174505"
    }
    gradient: uncheckedGradient

    MouseArea{
        id: buttonMouseArea
        anchors.fill: parent //область для приема событий от мышки будет занимать всю родительскую область
        onClicked: {
            if (choice_type=="once") { //if multiple is false, then from has radio buttons
                if (option_bar.selectedBtnID != undefined ){
                    option_bar.selectedBtnID.state = ""
                }
                option_bar.selectedBtnID = parent;
                option_bar.selectedBtnID.state = "checked"
                console.log("Clicked: " + option_bar.selectedBtnID.label);
            }
            else if (choice_type == "sequence") {
                choice.state == "choosed" ? choice.state = "" : choice.state = "choosed";
                console.log(choice.liecolor);
                for(var i=0; i<sequence.children.length; ++i){
                    if (choice.state == "choosed"){
                        if  (sequence.children[i].color == "#808080"){
                            sequence.children[i].color = choice.liecolor;
                            i=sequence.children.length;
                        }
                    }
                    else {
                        if  (sequence.children[i].color == choice.liecolor){
                            sequence.children[i].color = "#808080";
                            i=sequence.children.length;
                        }
                    }
                }
            }
//            else if (choice_type == "colleration") {
//                if (!choice.parent.clicked){
//                    choice
//                }
////                choice.state == "checked" ? choice.state = "" : choice.state = "checked"
////                console.log(choice.parent.side);
//            }
            else if (choice_type == "multiple") { choice.state == "checked" ? choice.state = "" : choice.state = "checked" }
            else (console.log("unknown Choice's state"))
        }
    }

    property alias label: labelChoice.text
    property string choice_type: "multiple"
}
