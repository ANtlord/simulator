// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Gradient {
    id: gray
    GradientStop {
        id: topGradient
        position: 0.0
        color: "#9c9d9c"
    }
    GradientStop {
        id: bottomGradient
        position: 1.0
        color: "#3b4439"
    }
    property alias topColor: topGradient.color
    property alias bottomColor: bottomGradient.color
}
