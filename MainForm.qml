// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    id: mainForm
    width: 600
    height: 500
    Question {
        id: question
        objectName: "question"
        anchors.horizontalCenter: mainForm.horizontalCenter
        y: 20
    }
    children: [
        AuthForm {
            id: authForm
            objectName: "AuthForm"
            opacity: 0
        },
        ChoicePart {
            id: choice_bar
            objectName: "choice_bar"
            y: question.y + question.height+20
            opacity: 0
        },
        OptionPart {
            id: option_bar
            objectName: "option_bar"
            y: question.y + question.height+20
            opacity: 0
        },
        TextResponse {
            id: text_bar
            objectName: "text_bar"
            y: question.y + question.height+20
            height: parent.height - button_bar.height - question.height - 40
            opacity: 0
        },
        SequencePart{
            id: sequence_bar
            objectName: "sequence_bar"
            y: question.y + question.height+20
            height: parent.height - button_bar.height - question.height - 40
            opacity: 0
        }
    ]

    ButtonBar{
        id: button_bar
        objectName: "button_bar"
        x: (parent.width-button_bar.width)/2
        y: mainForm.height-62
    }

    Rectangle {
        id:helpScreen
        width:mainForm.width; height:mainForm.height-100
        objectName: "HelpField"
        opacity: 0
        property alias label: helpText.text

        Flickable {
            id: flickArea
            anchors.fill: parent
            contentWidth: helpText.width; contentHeight: helpText.height
            flickableDirection: Flickable.VerticalFlick
            clip: true

            TextEdit{
                id: helpText
                wrapMode: TextEdit.Wrap
                width:helpScreen.width;
                readOnly:true
            }
        }
    }
}
