// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1


Rectangle {
    id: option_bar
    width: parent.width
    height: 450
    property variant selectedBtnID: selectedRadioBtn.selectedObj
//    property variant somMetric: firstOption
//    property variant somUS: secondOption

    QtObject {
        id: selectedRadioBtn
        property variant selectedObj
    }

    children : [
        Choice {
            id: firstOption
            label: "Парадигма объектно-ориентированного программирования состоит из наследования, инкапсуляции, полиморфизма"
            y: 0
            choice_type: "once"
            state: ""
        },
        Choice {
            id: secondOption
            label: "Парадигма объектно-ориентированного программирования состоит из наследования, инкапсуляции, полиморфизма asdasd"
            y: firstOption.y + firstOption.height + 20
            choice_type: "once"
            state: ""
        },
        Choice {
            id: thirdOption
            label: "Парадигма объектно-ориентированного программирования состоит из наследования, инкапсуляции, полиморфизма asdasd"
            y: secondOption.y + secondOption.height + 20
            choice_type: "once"
            state: ""
        },
        Choice {
            id: fourthOption
            label: "Парадигма объектно-ориентированного программирования состоит из наследования, инкапсуляции, полиморфизма asdasd"
            y: thirdOption.y + thirdOption.height + 20
            choice_type: "once"
            state: ""
        }
    ]
}
