// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Gradient {
    id: triGradient
    GradientStop {
        id: topGradient
        position: 0.0
        color: lightGray
    }
    GradientStop {
        id: middleGradient
        position: 0.5
        color: darkGray
    }
    GradientStop {
        id: bottomGradient
        position: 1.0
        color: lightGray
    }
    property alias topColor: topGradient.color
    property alias middleColor: middleGradient.color
    property alias bottomColor: bottomGradient.color
}
