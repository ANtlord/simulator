#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QTextCodec* codec = QTextCodec::codecForName("utf-8");
    QTextCodec::setCodecForTr(codec);
    QTextCodec::setCodecForCStrings(codec);
    QTextCodec::setCodecForLocale(codec);

    this->CurrentPoll = 1;
    ui = new QDeclarativeView;
    ui->setSource(QUrl::fromLocalFile("MainForm.qml"));
    setCentralWidget(ui);
    ui->setResizeMode(QDeclarativeView::SizeRootObjectToView);
    RootObj = ui->rootObject();
    ui->rootContext()->setContextProperty("window", this);
    this->Question = RootObj->findChild<QObject*>("question");
    this->PollsBar = NULL;

    QFile f("Book1.csv");
    MaxLevel = 1;
    if (f.open(QIODevice::ReadOnly)) {
        //file opened successfully
        QString data;
        while(!f.atEnd()){
            data = f.readLine();
            qDebug()<<data;
            data=data.remove("\n");
            QStringList vals = data.split(';');
            if(vals[0].toShort() > MaxLevel)
                MaxLevel = vals[0].toShort();
            this->Polls.append(vals);
        }
        Level = 1;
        qDebug()<<"MaxLevel is "<<MaxLevel;
        f.close();
    }
    else {
        qDebug()<<"file not exists";
    }
    showAuthorise();
}

void MainWindow::showQuestion() {

    while (Polls[CurrentPoll][0].toShort()!=Level) ++CurrentPoll;

    QuestionType = Polls[CurrentPoll][1];
    qDebug()<<QuestionType;
    PollsBar = RootObj->findChild<QObject*>(QuestionType);

    PollsBar->setProperty("opacity", "1");
    Question->setProperty("label", Polls[CurrentPoll][2]);
    if (QuestionType!="text_bar")
        for (int i=0; i<4; ++i)
            PollsBar->children().at(i)->setProperty("label", Polls[CurrentPoll][i+3]);


}


void MainWindow::showAuthorise() {
    QObject * AuthForm = RootObj->findChild<QObject*>("AuthForm");
    QObject * ButtonBar = RootObj->findChild<QObject*>("button_bar");
    Question->setProperty("label", "Представьтесь");
    AuthForm->setProperty("opacity", "1");
    ButtonBar->setProperty("state", "AuthState");
}

void MainWindow::showHelp(){
    QObject * HelpField = RootObj->findChild<QObject*>("HelpField");
    if (HelpField->property("opacity").toString() == "0"){
        QFile HelpFile("help.txt");
        QString HelpText;
        if (HelpFile.open(QIODevice::ReadOnly)) {
            while(!HelpFile.atEnd())
                HelpText += QString::fromAscii(HelpFile.readLine());
            HelpFile.close();

            HelpField->setProperty("label", HelpText);
            HelpField->setProperty("opacity", "1");
            qDebug()<<HelpText;
        }
        else qDebug()<<"File wasn't read";
    }
    else
        HelpField->setProperty("opacity", "0");
}

void MainWindow::authorise() {
    QObject * AuthText = RootObj->findChild<QObject*>("AuthText");
    QString AuthLogin = AuthText->property("text").toString();

    Report.setFileName(QDir::currentPath()+"/Reports/"+AuthLogin+".csv");

    if (Report.open(QIODevice::WriteOnly)) {
        qDebug()<<"File was created with name "+QDir::currentPath()+"/Repotrs/"+AuthLogin+".csv";
        Report.close();

        QObject * AuthForm = RootObj->findChild<QObject*>("AuthForm");
        QObject * ButtonBar = RootObj->findChild<QObject*>("button_bar");

        AuthForm->setProperty("opacity", "0");
        ButtonBar->setProperty("state", "MainState");
        showQuestion();
    }
    else { qDebug()<<"File wasn't created"; }
}

void MainWindow::hideQuestion() {
    if (PollsBar) {
    PollsBar->setProperty("opacity", "0");
        if (QuestionType=="text_bar")
                PollsBar->setProperty("label", "");
        else {
            for (int i=0; i<4; ++i)
                PollsBar->children().at(i)->setProperty("state", "");

            if (QuestionType=="sequence_bar") {
                QObject * Seqeunce = RootObj->findChild<QObject*>("sequence");
                for (int i=0; i<4; ++i)
                    Seqeunce->children().at(i)->setProperty("color", "gray");
            }
        }
    }
    else qDebug()<<"PollsBar was not initilized";
}

void MainWindow::Resume() {
    QObject * ButtonBar = RootObj->findChild<QObject*>("button_bar");
    ButtonBar->setProperty("state", "ResumeState");
    QVector <QStringList> Responses;

    if (Report.open(QIODevice::ReadOnly)) {
        unsigned int Counter(0);

        while(!Report.atEnd()){
            QString ResumeData = Report.readLine();
            qDebug()<<ResumeData;

            ResumeData=ResumeData.remove("\n");
            QStringList ResponseVals = ResumeData.split(';');
            Responses.append(ResponseVals);
        }
        Report.close();
        QString Output;
        for(int i=0; i<Responses.size(); ++i){
            if (Responses[i][3]=="1") ++Counter;

            Output+=Responses[i][0]+": "+Responses[i][3]+"\n\n";
        }


        if (Report.open(QIODevice::Append)) {
            qDebug()<<Counter<<" "<<CurrentPoll;
            float Result = ((float)Counter/(float)(CurrentPoll-1));
            Result*=100;
            Report.write(("Процент правильных ответов:;"+QString::number(Result)).toAscii());

            Question->setProperty("fontsize", "11");
            Question->setProperty("label", Output+"\n"+"Процент правильных ответов: "+QString::number(Result));
            Report.close();
        }
    }
    else {
        qDebug()<<"file not exists";
    }
}

void MainWindow::getNextQuestion() {
    hideQuestion();
    ++CurrentPoll;
    if (CurrentPoll<Polls.size())
        showQuestion();
    else Resume();
}

void MainWindow::responseQuestion() {
    if (Report.open(QIODevice::Append)) {
        QString Response;
        Report.write((Question->property("label").toString()+";").toAscii());
        if (QuestionType == "choice_bar")
            getChoiceQuestionData(Response);
        else if (QuestionType == "option_bar")
            getOptionQuestionData(Response);
        else if (QuestionType == "text_bar")
            getTextQuestionData(Response);
        else if (QuestionType == "sequence_bar")
            getSequenceQuestionData(Response);
        Response=Response.trimmed();
        Polls[CurrentPoll][7] = Polls[CurrentPoll][7].trimmed();

        qDebug()<<("Текущий правильный ответ "+(Polls[CurrentPoll][7]));
        qDebug()<<("Текущий выбранный ответ "+(Response));


        Report.write((Response+";").toAscii());
        Report.write((Polls[CurrentPoll][7]+";").toAscii());
        qDebug()<<(Polls[CurrentPoll][7]+" == "+Response);

        bool Correctly = (Polls[CurrentPoll][7] == Response);
        qDebug()<<Correctly;
        Report.write(QString::number(Correctly).toAscii());
        Report.write("\n");
        Report.close();

        if (Correctly == false){
            QMessageBox * msg = new QMessageBox();
            msg->setText("Правильный ответ "+(Polls[CurrentPoll][7]));
            msg->setGeometry(300,300,400,300);
            msg->show();
            return;
        }

        if (Correctly == true && Level < MaxLevel) {
            qDebug()<<"LevelUp";
            ++Level;
        }
        else if (Level > 1){
            --Level;
        }
    }
    else qDebug()<<"Can't open file for append";

    getNextQuestion();
}

void MainWindow::getSequenceQuestionData(QString &Response){
    QObject * Sequence = RootObj->findChild<QObject*>("sequence");
    for (short j=0; j<4; ++j)
        for (short i=0; i<4; ++i)
            if (Sequence->children().at(i)->property("color").toString() == "#ff0000" && j==0)
                Response+=(QString::number(i+1));
            else if (Sequence->children().at(i)->property("color").toString() == "#00ff00" && j==1)
                Response+=(","+QString::number(i+1));
            else if (Sequence->children().at(i)->property("color").toString() == "#0000ff" && j==2)
                Response+=(","+QString::number(i+1));
            else if (Sequence->children().at(i)->property("color").toString() == "#ffff00" && j==3)
                Response+=(","+QString::number(i+1));
}

void MainWindow::getTextQuestionData(QString &Response){
    Response = PollsBar->property("label").toString();
}

void MainWindow::getOptionQuestionData(QString &Response){
    for (int i=0; i<4; ++i)
        if (PollsBar->children().at(i)->property("state") != ""){
            Response=QString::number(i+1);
            i=4;
        }
}

void MainWindow::getChoiceQuestionData(QString &Response) {
    for (int i=0; i<4; ++i)
        if (PollsBar->children().at(i)->property("state") != "")
            Response+=(" "+QString::number(i+1));
    Response = Response.trimmed();
}

void MainWindow::alertMessage(){
    qDebug()<<"JUMPDAFACKUP";
}

MainWindow::~MainWindow()
{
    delete PollsBar;
    delete Question;
    delete RootObj;
    delete this->ui;
}
