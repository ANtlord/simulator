#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtDeclarative/QDeclarativeView>
#include <QGraphicsObject>
#include <QtGui>
#include <QDeclarativeContext>

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    Q_INVOKABLE void alertMessage();
    Q_INVOKABLE void getNextQuestion();
    Q_INVOKABLE void responseQuestion();
    Q_INVOKABLE void authorise();
    Q_INVOKABLE void showHelp();
    QVector <QStringList> Polls;
private:
    QFile Report;
    QObject * Question;
    QObject * PollsBar;
    QString QuestionType;
    short CurrentPoll;
    short Level;
    short MaxLevel;
    QDeclarativeView *ui;
    void getChoiceQuestionData(QString &Response);
    void getOptionQuestionData(QString &Response);
    void getTextQuestionData(QString &Response);
    void getSequenceQuestionData(QString &Response);
    void Resume();
    void showAuthorise();
    void showQuestion();
    void hideQuestion();
    QObject* RootObj;

    //methods
};

#endif // MAINWINDOW_H
